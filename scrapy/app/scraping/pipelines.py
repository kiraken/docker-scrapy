# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html
import scrapy
import psycopg2
import re

class ScrapingPipeline(object):
    def process_item(self, item, spider):
        return item

# PostgreSQLへの保存
class PostgresPipeline(object):
    def open_spider(self, spider: scrapy.Spider):
        # コネクションの開始
        url = spider.settings.get('POSTGRESQL_URL')
        self.conn = psycopg2.connect(url)
        self.conn.autocommit = False

    def close_spider(self, spider: scrapy.Spider):
        # コネクションの終了
        self.conn.close()

    def process_item(self, item: scrapy.Item, spider: scrapy.Spider):
        self.cursor = self.conn.cursor()
        sql = "INSERT INTO scrapydb_item (status,url,img,title,introduction,creator,scraping_at,created_at,updated_at) VALUES (%s, %s, %s, %s, %s, %s, current_timestamp ,current_timestamp,current_timestamp)"
        self.cursor.execute(sql, ('draft',item['url'], item['img'], item['title'],item['introduction'], item['creator']))
        self.conn.commit()
        self.cursor.close()

        return item
