# -*- coding: utf-8 -*-
import scrapy
from scraping.items import CreemaItem
from scrapy_splash import SplashRequest
from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.contrib.linkextractors import LinkExtractor
from scrapy.selector import HtmlXPathSelector
import logging
from twisted.internet.defer import setDebugging
import re
import time

class CreemaSpider(scrapy.Spider):
    name = 'creema'
    allowed_domains = ['www.creema.jp']
    start_urls = ['https://www.creema.jp/listing'] # URLのリスト
    #start_urls = ['https://www.creema.jp/item/5021677/detail'] # URLのリスト

    allow_list = [r'/item/[\d]+/detail'] # 'www.creema.jp/item'以下を正規表現でマッチング
    #restrict_list = ['//h3[@title="{0}"]'.format(i) for i in PRODUCT_LIST] # スパイダーがスナイポするHTML要素

    rules = (
        Rule(LinkExtractor(
                allow=allow_list,
                #restrict_xpaths=restrict_list,
                unique=True,
        ), callback='parse', follow=True),
    )

    logging.basicConfig(filename='example.log',level=logging.DEBUG)

    def start_requests(self):
        for url in self.start_urls:
            yield SplashRequest(url, self.parse, args={'wait': 3})

    def parse(self,response):

        #creemaの検索リストページからURLを取得
        for link in response.xpath('//div[@class="p-items-article-list__link"]/a/@href').extract():
            #yield response.follow("https://www.creema.jp" + link, callback=self.parse_item)
            yield scrapy.Request(response.urljoin(link), callback=self.parse_item)

        next_page = response.xpath('//*[@id="two-col-main"]/article[2]/div/div/div/div[12]/a/@href').extract_first()

        if next_page_url is not None:
            logging.debug("----------------------------")
            logging.debug("[NEXT PAGE]:" + "https://www.creema.jp" + str(next_page_url))
            logging.debug("----------------------------")
            yield scrapy.Request(response.urljoin(next_page_url), callback=self.parse)


    def parse_item(self, response):
        time.sleep(5)
        logging.debug("----------------------------")
        logging.debug("[NEXT LINK]:" + str(response.url))
        logging.debug("----------------------------")

        item = CreemaItem()

        item['url'] = response.url
        item['img'] = response.xpath('//*[@id="js-item-detail"]//div[@class="p-item-detail-thumbnail"]//img/@data-url').extract_first()
        item['title'] = response.xpath('//*[@id="js-item-detail"]/article/h2').xpath("string()").extract_first()
        item['introduction'] = response.xpath('//*[@id="introduction"]/div/div').xpath("string()").extract_first()
        item['creator'] = response.xpath('//*[@id="js-item-detail"]/aside/div[2]/div[1]/div[2]/h4/a/span').xpath("string()").extract_first()

        """
        logging.debug("----------------------------")
        logging.debug("[item.url] " + item['url'] )
        logging.debug("[item.img] " + item['img'] )
        logging.debug("[item.creator] " + item['creator'] )
        ogging.debug("[item.title] " + item['title'] )
        logging.debug("[item.introduction] " + item['introduction'] )
        logging.debug("----------------------------")
        """

        yield item
