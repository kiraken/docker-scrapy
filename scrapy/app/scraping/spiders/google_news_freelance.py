# -*- coding: utf-8 -*-
import scrapy
from scraping.items import GoogleNewsDetailItem
from scrapy_splash import SplashRequest
import logging
from twisted.internet.defer import setDebugging
import re

class GoogleNewsFreelanceSpider(scrapy.Spider):
    setDebugging(True)
    name = 'google_news_freelance'
    allowed_domains = ['www.google.co.jp']
    start_urls = ['https://www.google.co.jp/search?hl=ja&source=lnms&tbm=nws&sa=X&q=フリーランス']
    #start_urls = ['https://prtimes.jp/']
    logging.basicConfig(filename='example.log',level=logging.DEBUG)

    def start_requests(self):
        for url in self.start_urls:
            yield SplashRequest(url, self.parse, args={'wait': 3})

    def parse(self, response):
        #--------------------------------------------------
        PAGE_LOOP_MAX = 3
        #--------------------------------------------------
        page_loop_num = 0
        #--------------------------------------------------
        #logging.debug("----------------------------")
        #logging.debug("html body:")
        #logging.debug(str(response.body))
        #logging.debug("----------------------------")
        for topic in response.css('.g'):
            item = GoogleNewsDetailItem()
            item['url'] = topic.css('a::attr(href)').extract_first()
            item['headline'] = topic.css('a.lLrAF').xpath("string()").extract_first()
            item['title'] = topic.css('a.lLrAF').xpath("string()").extract_first()
            item['body'] = topic.css('div.st').xpath("string()").extract_first()

            logging.debug("----------------------------")
            logging.debug("[item.url] " + item['url'] )
            logging.debug("[item.headline] " + item['headline'] )
            logging.debug("[item.title] " + item['title'] )
            logging.debug("[item.bdoy] " + item['body'] )
            logging.debug("----------------------------")
            yield item

        next_page = response.xpath('//*[@id="pnnext"]').css('a::attr(href)').extract_first()
        #next_page = response.css('a#pnnext::attr(href)').extract_first()
        logging.debug("----------------------------")
        logging.debug("next_page:" + str(next_page))
        logging.debug("----------------------------")
        #最大ループ回数制限
        if page_loop_num > PAGE_LOOP_MAX :
            next_page = None

        if next_page is not None:
            page_loop_num += 1
            yield response.follow(next_page, callback=self.parse)
            #next_page = response.urljoin(next_page)
            #yield scrapy.Request(next_page, callback = self.parse)
