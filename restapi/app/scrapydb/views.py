from django.shortcuts import render
import django_filters
from rest_framework import viewsets, filters
from .serializer import EntrySerializer,ItemSerializer
from .models import Entry,Item

# Create your views here.
class EntryViewSet(viewsets.ModelViewSet):
    queryset = Entry.objects.all()
    serializer_class = EntrySerializer

class ItemViewSet(viewsets.ModelViewSet):
    queryset = Item.objects.all()
    serializer_class = ItemSerializer
