# coding: utf-8

from rest_framework import routers
from .views import EntryViewSet,ItemViewSet


router = routers.DefaultRouter()
router.register(r'entries', EntryViewSet)
router.register(r'items', ItemViewSet)
