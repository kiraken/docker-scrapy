# coding: utf-8

from rest_framework import serializers
from .models import Entry,Item
import logging

logger=logging.getLogger('django')

class EntrySerializer(serializers.ModelSerializer):
    class Meta:
        model = Entry
        fields = (
                  'status',
                  'url',
                  'headline',
                  'title',
                  'body',
                  'created_at',
                  'updated_at',
                  )

class ItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = Item
        fields = (
                  'status',
                  'url',
                  'img',
                  'title',
                  'creator',
                  'introduction',
                  'scraping_at',
                  )
