from django.db import models

# Create your models here.

class Entry(models.Model):
    STATUS_DRAFT = "draft"
    STATUS_PUBLIC = "public"
    STATUS_SET = (
            (STATUS_DRAFT, "下書き"),
            (STATUS_PUBLIC, "公開中"),
    )
    status = models.CharField(choices=STATUS_SET, default=STATUS_DRAFT, max_length=8)
    url = models.TextField()
    headline = models.TextField()
    title = models.TextField()
    body = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

class Item(models.Model):
    STATUS_DRAFT = "draft"
    STATUS_PUBLIC = "public"
    STATUS_SET = (
            (STATUS_DRAFT, "下書き"),
            (STATUS_PUBLIC, "公開中"),
    )
    status = models.CharField(choices=STATUS_SET, default=STATUS_DRAFT, max_length=8)
    url = models.TextField()
    img = models.TextField()
    title = models.TextField()
    introduction = models.TextField()
    creator = models.TextField()
    scraping_at = models.DateTimeField(auto_now_add=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
