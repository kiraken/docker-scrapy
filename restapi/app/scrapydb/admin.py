from django.contrib import admin

# Register your models here.

from .models import Entry,Item
from .adminResources import EntryResource,ItemResource
from import_export.admin import ImportExportModelAdmin

#@admin.register(Entry)
#class Entry(admin.ModelAdmin):
#    pass
@admin.register(Entry)
class Entry(ImportExportModelAdmin):
    resource_class = EntryResource
    list_display = (
        'status',
        'url',
        'headline',
        'title',
        'body',
        'created_at',
        )
    list_filter = ['status',]
    search_fields = ['url','title','body',]

@admin.register(Item)
class Item(ImportExportModelAdmin):
    resource_class = ItemResource
    list_display = (
        'status',
        'url',
        'title',
        'introduction',
        'creator',
        'scraping_at',
        )
    list_filter = ['status','creator']
    search_fields = ['url','title','introduction',]
