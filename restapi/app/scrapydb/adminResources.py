# Register your models here.
from .models import Entry,Item

#django-import-export CSV UPLODAD/DOWNLOAD
from import_export import resources

class EntryResource(resources.ModelResource):

    class Meta:
        model = Entry
        skip_unchanged = True
        report_skipped = False
        import_id_fields = ('id', )

class ItemResource(resources.ModelResource):

    class Meta:
        model = Item
        skip_unchanged = True
        report_skipped = False
        import_id_fields = ('id', )
