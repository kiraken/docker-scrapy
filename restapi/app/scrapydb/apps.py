from django.apps import AppConfig


class ScrapydbConfig(AppConfig):
    name = 'scrapydb'
